docker run \
  --name postgres \
  --rm\
  -p 32772:5432\
  --env POSTGRES_PASSWORD=calachat_local\
  --env POSTGRES_USER=calachat_local\
  --env POSTGRES_DB=calachat_local\
   postgres:14.1-alpine