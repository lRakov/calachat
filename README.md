## Calachat

### Documentation

#### Description

#### API

#### Persistence

##### DB Schema

![DB Schema](./docs/assets/db_schema.png)

### TODO

- Documentation
  - General description
  - api description
  - ~~db schema~~

- Code Quality
  - repository settings 
  - ~~pipeline stages for linters/sonar~~

- QA
  - frontend tests
    - unit
  - backend tests
    - unit
    - integrational
  - E2E tests

- DEVOPS
  - CI/CD
  - IaaC for
    - production environment
    - developing environment(s)
  - canary deployment

- Production ready features
  - log storage(elk)
  - ~~health checks~~
  - analytics & metrics & alerts
    - frontend
    - backend
    - infrastructure

- Security
  - ~~TLS~~
  - Storage for k8s secrets
  - move to Helm
  - switch to server-side google auth(to support browser's private mode) 

- Product features
  - [UI] reset auth context on 401 http status
  - [UI] ~~client-side user input validation~~
  - [UI] server-side user input validation
  - [UI] ~~smoke test on mobile devices and adjust to them~~ 
  - [UI] i18n
  - [UI] ~~manual usability check on slow connection profile~~