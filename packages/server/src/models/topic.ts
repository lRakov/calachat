import {Topic, TopicMember, TopicStatus} from '../gen-types';
import {db, TopicMemberEntity, UserEntity} from "../persist/db";
import * as subscriptions from "../subscriptions";
import {isConflict} from "./util";

export async function getByUserId(
    userId: number,
    limit = 20
): Promise<Topic[]> {
    const members = await db('topicMember')
        .where({userId: userId})
        .withRelations(db('user'), 'userId', 'id');
    return db('topic')
        .whereIn('id', members.map((member: any) => member.topicId))
        .orderBy('displayName')
        .limit(limit)
        .then(topics => {
            return topics.map(topic => {
                return {
                    ...topic,
                    members: members
                        .filter((member: any) => member.topicId === topic.id)
                        .flatMap((member: any) => member.user as UserEntity)
                };
            });
        })
}

export async function createTopic(input: { displayName: string }): Promise<Topic> {
    try {
        return await db.insert({
            ...input,
            status: TopicStatus.Active,
            created_at: new Date().toISOString(),
            updated_at: new Date().toISOString(),
        }).into('topic')
            .onConflict('displayName')
            .ignore()
            .returning('*')
            .firstOrFail()
            .then(topic => ({...topic, members: []}));
    }
    catch (e) {
        if (isConflict(e)) {
            return (await getByText(input.displayName, 1))[0];
        }
        throw e;
    }
}

export async function joinTopic(input: {
    userId: number,
    topicId: number
}): Promise<TopicMember> {
    const result = await upsertTopic(input);
    await subscriptions.publishTopicAdded(result);
    return result;
}

export async function getByText(
    text: string | undefined,
    limit = 20
): Promise<Topic[]> {
    if (!text) {
        return []
    }
    return db('topic')
        .returning(['id', 'displayName', 'status'])
        .where('displayName', 'ilike', `%${text}%`)
        .orderBy('displayName')
        .limit(limit)
        .then(topics => topics.map(topic => ({...topic, members: []})))
}

function upsertTopic(input: {
    userId: number,
    topicId: number
}): Promise<TopicMemberEntity> {
    return db.insert(<TopicMember>{
        ...input,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString(),
    }).into('topicMember')
        .onConflict(['userId', 'topicId'])
        .merge()
        .returning('*')
        .firstOrFail();
}