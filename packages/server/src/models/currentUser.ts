import {CurrentUser, IUser} from '../gen-types';
import {db} from "../persist/db";
import {TokenPayload} from 'google-auth-library';
import {updateUserByGoogle} from "./user";

export async function getByUserId(id: number): Promise<CurrentUser> {
    return db.table('user')
        .where('id', id)
        .firstOrFail()
}

export async function updateByGoogle(payload: TokenPayload): Promise<IUser> {
    return updateUserByGoogle(payload)
}