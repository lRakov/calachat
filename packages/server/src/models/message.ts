import {Message, MessagesByInput} from '../gen-types';
import {db, MessageEntity} from "../persist/db";
import * as subscriptions from "../subscriptions";

export function getBy(input: MessagesByInput): Promise<Message[]> {
    const query = db('message')
        .where({ topicId: input.topicId})
        .orderBy('id', 'asc');
    if (input.minId) {
        query.andWhere('id', '>', input.minId)
    }
    if (input.maxId) {
        query.andWhere('id', '<', input.maxId)
    }
    if (input.limit) {
        query.limit(input.limit)
    }
    return query.returning('*')
}

export async function createMessage(input: {
    topicId: number,
    sourceId: number,
    text: string,
}): Promise<Message> {
    const result = await insertMessage(input);
    await subscriptions.publishMessage(result);
    await subscriptions.publishTopicUpdate({
        hasNewMessages: true,
        topicId: input.topicId,
    })
    return result;
}

function insertMessage(input: {
    topicId: number,
    sourceId: number,
    text: string,
}): Promise<MessageEntity> {
    return db.insert({
        ...input,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString(),
    }).into('message')
        .returning('*')
        .firstOrFail();
}