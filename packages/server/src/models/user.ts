import {IUser} from '../gen-types';
import {db, UserEntity} from "../persist/db";
import {TokenPayload} from 'google-auth-library';


export async function getBy(id: number): Promise<UserEntity> {
    return db('user')
        .where({id: id})
        .returning('*')
        .firstOrFail();
}

export async function updateUserByGoogle(payload: TokenPayload): Promise<IUser> {
    const user = {
        displayName: payload.name || (payload.given_name + ' ' + payload.family_name).trim(),
        firstName: payload.given_name,
        lastName: payload.family_name,
        lastSeen: new Date(),
        avatar: payload.picture,
        googleSub: payload.sub,
    };
    return db
        .insert(user)
        .into('user')
        .onConflict('googleSub')
        .merge()
        .returning('*')
        .firstOrFail();
}