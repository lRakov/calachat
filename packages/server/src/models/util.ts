// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function isConflict(error: any): boolean {
    return error instanceof Error && error.message.includes('Query has no results');
}