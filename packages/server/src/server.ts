import {ApolloServer, gql} from 'apollo-server-express';
import {GraphQLError, GraphQLSchema} from 'graphql';
import {gqlSchema} from 'calachat_common';
import {Logger} from 'tslog';
import gqlResolvers from './resolvers';
import {buildContext} from './context';
import {resolvers as scalarResolvers, typeDefs as scalarTypeDefs} from 'graphql-scalars';
import {ApolloServerPluginDrainHttpServer} from 'apollo-server-core';
import express from 'express';
import {createServer, Server} from 'http';
import {makeExecutableSchema} from '@graphql-tools/schema';
import {prepareWsServer} from './util/wsUtil';
import CustomError401Plugin from './util/CustomError401Plugin';
import {allowedOrigin, environment, serverPort} from './config';
import {createLightship} from 'lightship';

const log: Logger = new Logger();

const lightship = createLightship();

async function launchServer(port: number): Promise<Server> {
    const schema: GraphQLSchema = makeExecutableSchema({
        typeDefs: [gql`${scalarTypeDefs}`, gqlSchema],
        resolvers: {...scalarResolvers, ...gqlResolvers},
    });
    const app = express();
    const httpServer = createServer(app);
    const apolloServer = new ApolloServer({
        schema,
        formatError: gqlErrorHandler,
        context: buildContext,
        plugins: [
            ApolloServerPluginDrainHttpServer({httpServer: httpServer}),
            CustomError401Plugin,   // dirty workaround
        ]
    });
    if (environment !== 'development') {
        app.use(express.static(__dirname + '/../../ui/build'));
    }
    await apolloServer.start()
        .then(() => {
            apolloServer.applyMiddleware({
                app: app,
                cors: {
                    origin: allowedOrigin,
                    credentials: true
                }
            });
        });
    const server = httpServer.listen(port, () => {
        prepareWsServer(httpServer, schema)
        lightship.signalReady();
    }).on('error', () => {
        lightship.shutdown();
    });
    lightship.registerShutdownHandler(() => {
        server.close();
    });
    return server;
}

const gqlErrorHandler = (err: GraphQLError) => {
    log.error('Error while running resolver', err);

    // TODO: introduce custom error instances
    return new Error('Internal server error');
};

async function main() {
    const port = serverPort;
    await launchServer(port)
        .then(() =>
            log.info(`Server is listening on port ${port}`)
        );
}

if (require.main === module) {
    log.info('Server is starting')
    main().then(() => {
        log.info('Server has started')
    });
}