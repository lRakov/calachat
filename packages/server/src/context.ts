import * as currentUserModel from './models/currentUser';
import * as topicsModel from './models/topic';
import * as messagesModel from './models/message'
import * as userModel from './models/user'
import {AuthResult, CurrentUser} from "./gen-types";
import {TokenPayload} from 'google-auth-library';
import {extractJwtPayloadForHttp, signJwt, verifyGoogleTokenId} from './util/authUtil'
import {AuthenticationError} from 'apollo-server-express';

export interface Context {
    models: {
        topics: typeof topicsModel;
        messages: typeof messagesModel;
        users: typeof userModel;
    };
    googleAuth(tokenId: string): Promise<AuthResult>;
    authorize(): Promise<CurrentUser>
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export async function buildContext({req}: any): Promise<Context> {
    return {
        models: {
            topics: topicsModel,
            messages: messagesModel,
            users: userModel,
        },
        async googleAuth(tokenId: string): Promise<AuthResult> {
            try {
                const payload: TokenPayload = await verifyGoogleTokenId(tokenId);
                const user = await currentUserModel.updateByGoogle(payload);
                const authToken = signJwt(user);
                return {
                    success: true,
                    userId: user.id,
                    authToken: authToken,
                };
            }
            catch (e: any) {
                return {
                    success: false,
                    error: ('message' in e) ? e.message : '',
                };
            }
        },
        async authorize(): Promise<CurrentUser> {
            try {
                const jwtTokenPayload = extractJwtPayloadForHttp(req);
                return currentUserModel.getByUserId(jwtTokenPayload.userId);
            }
            catch (e) {
                throw new AuthenticationError('incorrect token');
            }
        },
    };
}