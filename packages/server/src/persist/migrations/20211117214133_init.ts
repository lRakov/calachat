import {Knex} from 'knex';
import {TopicStatus} from "../../gen-types";

export async function up(knex: Knex): Promise<void> {
    // TODO check lc_collate, lc_ctype, encoding
    return knex.schema
        .raw(`ALTER DATABASE ${knex.client.database()} SET timezone = 'UTC'`)
        .raw(`ALTER DATABASE ${knex.client.database()} SET DEFAULT_TRANSACTION_ISOLATION TO 'read committed'`)
        .createTable('user', function (table) {
            table.increments();
            table.string('displayName', 128).notNullable();
            table.string('firstName', 64).nullable();
            table.string('lastName', 64).nullable();
            table.string('avatar', 512).nullable();
            table.dateTime('lastSeen').notNullable();
            table.string('googleSub').nullable().unique().comment("an identifier for the user, unique among all Google accounts, see `TokenPayload.sub`");
            table.timestamps(false, true)
        })
        .createTable('topic', function (table) {
            table.increments().primary();
            table.string('displayName', 128).notNullable().unique();
            table.enum('status', [TopicStatus.Draft, TopicStatus.Active, TopicStatus.Muted, TopicStatus.Closed]).nullable();
            table.timestamps(false, true)
        })
        .createTable('topicMember', function (table) {
            table.increments()
            table.integer('topicId').notNullable().references('id').inTable('topic') // NOTE: index is redundant, there is a unique composite index on ['topicId', 'userId']
            table.integer('userId').notNullable().index().references('id').inTable('user')
            table.unique(['topicId', 'userId'])
            table.timestamps(false, true)
        })
        .createTable('message', function (table) {
            table.increments();
            table.integer('topicId').notNullable().index().references('id').inTable('topic')
            table.integer('sourceId').notNullable().index().references('id').inTable('user')
                .comment("source of the message in general")
            table.string('text', 1024).nullable();
            table.timestamps(false, true)
        })
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema
        .dropTable('message')
        .dropTable('topicMember')
        .dropTable('topic')
        .dropTable('user')
}