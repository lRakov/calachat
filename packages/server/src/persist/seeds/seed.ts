import {Knex} from "knex";
import {TopicStatus} from "../../gen-types";

export async function seed(knex: Knex): Promise<void> {
    const now = new Date().toISOString();
    await knex("topic").del();
    await knex("topic").insert([
        {displayName: "Topic1", status: TopicStatus.Active, created_at: now, updated_at: now},
        {displayName: "Topic2", status: TopicStatus.Active, created_at: now, updated_at: now},
        {displayName: "Topic3", status: TopicStatus.Active, created_at: now, updated_at: now},
    ]);
}