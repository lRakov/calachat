import createKnex from 'knex';
import 'knex-first-or-fail';
import 'knex-with-relations';
import config from './knexfile';
import {TopicStatus} from "../gen-types";

export const db = createKnex(config);

export interface UserEntity {
    id: number;
    firstName?: string;
    lastName?: string;
    displayName: string;
    avatar?: string;
    lastSeen: Date;
    googleSub?: string;
    created_at: Date;
    updated_at: Date;
}

export interface TopicEntity {
    id: number;
    displayName: string;
    created_at: string;
    updated_at: string;
    status: TopicStatus;
}

export interface TopicMemberEntity {
    id: number;
    topicId: number;
    userId: number;
    created_at: Date;
    updated_at: Date;
}

export interface MessageEntity {
    id: number;
    topicId: number;
    sourceId: number;
    text: string;
    created_at: Date;
    updated_at: Date;
}