import {Knex} from 'knex';

const config: Knex.Config = {
    client: 'pg',
    connection: {
        application_name:    process.env.DB_APPLICATION_NAME || 'calachat',
        connectionTimeout: +(process.env.DB_CONNECTION_TIMEOUT || 10),
        host:                process.env.DB_HOST || 'localhost',
        port:              +(process.env.DB_PORT || 32772),
        user:                process.env.DB_USER || 'calachat_local',
        password:            process.env.DB_PASSWORD || 'calachat_local',
        database:            process.env.DB_DATABASE || 'calachat_local',
        debug:               process.env.DB_DEBUG === 'true',
        ssl: (process.env.DB_SSL === 'true')
            ? {rejectUnauthorized: process.env.DB_SSL_REJECT_UNAUTHORIZED === 'true',}
            : false,
    },
    pool: {
        min: 2,
        max: 10,
/* TODO
        afterCreate: function (conn: any, done: any) {
            // in this example we use pg driver's connection API
            conn.query('SET timezone='UTC';', function (err: any) {
                if (err) {
                    // first query failed, return error and don't try to make next query
                    done(err, conn);
                } else {
                    // do the second query...
                    conn.query('SELECT set_limit(0.01);', function (err: any) {
                        // if err is not falsy, connection is discarded from pool
                        // if connection aquire was triggered by a query the error is passed to query promise
                        done(err, conn);
                    });
                }
            })
        }
*/
    },
    migrations: {
        extension: 'ts',
        tableName: 'knex_migrations',
        directory: 'migrations',
    },
    seeds: {
        extension: 'ts', //define work typescript to seeds
        directory: 'seeds',
    },
};

export default config;