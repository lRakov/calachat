import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';

dotenvExpand(dotenv.config());

export const environment = process.env.NODE_ENV || 'undefined';
export const allowedOrigin: string = process.env.ALLOWED_ORIGIN || 'undefined';
export const serverPort: number = +(process.env.SERVER_PORT || 'undefined');
export const jwtSecret: string = process.env.JWT_SECRET || 'undefined';
export const googleSignInClientId: string = process.env.GOOGLE_SIGN_IN_CLIENT_ID || 'undefined'