import {Message, TopicMember, TopicUpdate} from '../gen-types';
import {PubSub, ResolverFn, withFilter} from 'graphql-subscriptions';
import {EventEmitter} from 'events';

// TODO: replace PubSub by production ready implementation of PubSubEngine

const eventEmitter = new EventEmitter();
eventEmitter.setMaxListeners(9999);

const pubsub = new PubSub({
    eventEmitter: eventEmitter
});

enum SubscriptionKey {
    ON_TOPIC_ADDED = "ON_TOPIC_ADDED",
    ON_TOPIC_UPDATE = "ON_TOPIC_UPDATE",
    ON_MESSAGE_ADDED = "ON_MESSAGE_ADDED",
}

export const subscribeOnTopicAdded = (): ResolverFn => withFilter(
    () => pubsub.asyncIterator(SubscriptionKey.ON_TOPIC_ADDED),
    ({OnTopicAdded}: any, _, {extra}) => {
        return OnTopicAdded.userId === extra.userId
    },
);

export const publishTopicAdded = (member: TopicMember): Promise<void> => {
    return pubsub.publish(SubscriptionKey.ON_TOPIC_ADDED, {
        OnTopicAdded: member
    })
};

export const subscribeOnTopicUpdate = (): ResolverFn => withFilter(
    () => pubsub.asyncIterator(SubscriptionKey.ON_TOPIC_UPDATE),    // TODO: check if user is allowed to get updates
    ({OnTopicUpdate}, {topicId}) => {
        return OnTopicUpdate.topicId == topicId;
    },
);

export const publishTopicUpdate = (update: TopicUpdate): Promise<void> => {
    return pubsub.publish(SubscriptionKey.ON_TOPIC_UPDATE, {
        OnTopicUpdate: update
    })
};

export const subscribeOnMessageAdded = (): ResolverFn => withFilter(
    () => pubsub.asyncIterator(SubscriptionKey.ON_MESSAGE_ADDED),
    ({OnMessageAdded}, {topicId}) => {
        return OnMessageAdded.topicId == topicId    // NOTE: str vs numbers
    },
);

export const publishMessage = (message: Message): Promise<void> => {
    return pubsub.publish(SubscriptionKey.ON_MESSAGE_ADDED, {
        OnMessageAdded: message
    })
};