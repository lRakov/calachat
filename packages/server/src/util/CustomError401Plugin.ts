import {GraphQLError} from 'graphql';
import {ApolloServerPlugin} from 'apollo-server-plugin-base';

const CustomError401Plugin: ApolloServerPlugin = {
    async requestDidStart() {
        return {
            async willSendResponse({errors, response}) {
                if (response
                    && response.http
                    && errors
                    && errors.some((err: GraphQLError) => err.extensions && err.extensions.code === 'UNAUTHENTICATED')
                ) {
                    response.data = undefined;
                    response.http.status = 401;
                }
            }
        }
    }
};

export default CustomError401Plugin;