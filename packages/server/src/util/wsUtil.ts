import {GraphQLSchema} from 'graphql';
import {Logger} from 'tslog';
import * as http from 'http';
import {WebSocketServer} from 'ws';
import {useServer} from 'graphql-ws/lib/use/ws';
import {Context as WsContext, GraphQLExecutionContextValue} from 'graphql-ws';
import {extractJwtPayloadForWs} from "./authUtil";
import * as currentUserModel from "../models/currentUser";

const log: Logger = new Logger();

export type WsExtra = {
    userId: number
}

export function prepareWsServer(
    httpServer: http.Server,
    schema: GraphQLSchema
): void {
    const wsServer = new WebSocketServer({
        server: httpServer,
        path: '/subscriptions',
    });
    useServer({
        schema,
        context: (ctx: WsContext): GraphQLExecutionContextValue => ctx,
        onConnect: async (ctx: WsContext) => {
            const user = await currentUserModel.getByUserId(extractJwtPayloadForWs(ctx).userId);
            ctx.extra = <WsExtra>{
                userId: user.id
            }
            log.info(`user(id=${user.id}) subscription connected`)
            return true
        },
        onDisconnect: async (ctx: WsContext) => {
            const user = await currentUserModel.getByUserId(extractJwtPayloadForWs(ctx).userId);
            log.info(`user(id=${user.id}) subscription disconnected`)
        },
    }, wsServer);
}