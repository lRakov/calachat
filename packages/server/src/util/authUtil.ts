import {OAuth2Client, TokenPayload} from 'google-auth-library';
import {sign, verify} from 'jsonwebtoken';
import {IUser} from "../gen-types";
import {Context as WsContext} from 'graphql-ws';
import {IncomingMessage} from "http";
import {googleSignInClientId, jwtSecret} from "../config";

const googleOAuth2Client = new OAuth2Client(googleSignInClientId);

type JwtTokenPayload = {
    userId: number,
}

export async function verifyGoogleTokenId(token: string): Promise<TokenPayload> {
    return googleOAuth2Client.verifyIdToken({
        idToken: token,
        audience: googleSignInClientId,
    }).then(ticket => {
        const payload = ticket.getPayload();
        if (!payload) {
            throw Error('undefined payload')
        }
        return payload;
    });
}

export function signJwt(user: IUser): string {
    const payload: JwtTokenPayload = {
        userId: user.id
    };
    return sign(payload, jwtSecret);
}

export function verifyJwt(token: string): JwtTokenPayload {
    return verify(token, jwtSecret) as JwtTokenPayload
}

export function extractJwtPayloadForHttp(req: IncomingMessage): JwtTokenPayload {
    const token = (req.headers?.authorization || "").replace("Bearer ", "");
    return verifyJwt(token);
}

export function extractJwtPayloadForWs(ctx: WsContext): JwtTokenPayload {
    const token = ((ctx.connectionParams as any).authorization || "").replace("Bearer ", "");
    return verifyJwt(token);
}