import {Knex} from 'knex';
import {MessageEntity, TopicEntity, TopicMemberEntity, UserEntity} from "../persist/db";

// TODO: move to persist
declare module 'knex/types/tables' {
    interface Tables {
        user: UserEntity;
        user_composite: Knex.CompositeTableType<
            UserEntity,
            Pick<UserEntity, 'displayName'> & Partial<Pick<UserEntity, 'created_at' | 'updated_at'>>,
            Partial<Omit<UserEntity, 'id'>>
        >;
        topic: TopicEntity;
        topic_composite: Knex.CompositeTableType<
            TopicEntity,
            Pick<TopicEntity, 'displayName'> & Partial<Pick<TopicEntity, 'created_at' | 'updated_at'>>,
            Partial<Omit<TopicEntity, 'id'>>
            >;
        topicMember: TopicMemberEntity;
        topicMember_composite: Knex.CompositeTableType<
            TopicMemberEntity,
            Pick<TopicMemberEntity, 'topicId' | 'userId'> & Partial<Pick<TopicMemberEntity, 'created_at' | 'updated_at'>>,
            Partial<Omit<TopicEntity, 'id'>>
            >;
        message: MessageEntity;
        message_composite: Knex.CompositeTableType<
            MessageEntity,
            Pick<MessageEntity, 'topicId' | 'sourceId' | 'text'> & Partial<Pick<MessageEntity, 'created_at' | 'updated_at'>>,
            Partial<Omit<TopicEntity, 'id'>>
            >;
    }
}