import {AuthResult, CurrentUser, Message, QueryMessagesByArgs, Resolvers, Topic, TopicMember, User} from '../gen-types';
import {Context} from '../context';
import * as subscriptions from "../subscriptions";

const resolvers: Resolvers = {
    Query: {
        currentUser: async (_, __, context: Context): Promise<CurrentUser> => {
            return context.authorize()
        },

        topics: async (_, __, context: Context): Promise<Topic[]> => {
            const user = await context.authorize();
            return context.models.topics.getByUserId(user.id);
        },
        topicsBy: async (_, {text}, context: Context): Promise<Topic[]> => {
            await context.authorize();
            return context.models.topics.getByText(text || undefined);
        },

        messagesBy: async (_, args: QueryMessagesByArgs, context: Context): Promise<Message[]> => {
            await context.authorize();
            return context.models.messages.getBy(args.input);
        },

        userBy: async (_, {userId}, context: Context): Promise<User> => {
            await context.authorize();
            return context.models.users.getBy(userId);
        },
    },
    Mutation: {
        doAuth: async (_, {tokenId}, context: Context): Promise<AuthResult> => {
            return context.googleAuth(tokenId || '')
        },

        doCreateTopic: async (_, {displayName}, context: Context): Promise<Topic> => {
            await context.authorize();
            return context.models.topics.createTopic({displayName: displayName})
        },

        doCreateMessage: async (_, {topicId, text}, context: Context): Promise<Message> => {
            const user = await context.authorize();
            return context.models.messages.createMessage({
                topicId: topicId,
                sourceId: user.id,
                text: text,
            })
        },

        doJoinTopic: async (_, {topicId}, context: Context): Promise<TopicMember> => {
            const user = await context.authorize();
            return context.models.topics.joinTopic({
                userId: user.id,
                topicId: topicId
            })
        },
    },
    Subscription: {
        OnTopicAdded: {
            subscribe: subscriptions.subscribeOnTopicAdded()
        },
        OnTopicUpdate: {
            subscribe: subscriptions.subscribeOnTopicUpdate()
        },
        OnMessageAdded:  {
            subscribe: subscriptions.subscribeOnMessageAdded()
        },
    }
};

export default resolvers;