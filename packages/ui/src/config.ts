import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';

dotenvExpand(dotenv.config());

export const googleSignInClientId: string = process.env.REACT_APP_GOOGLE_SIGN_IN_CLIENT_ID || 'undefined';
export const graphqlEndpoint: string = process.env.REACT_APP_SERVER_GRAPHQL_ENDPOINT || `undefined`;
export const subscriptionsEndpoint: string = process.env.REACT_APP_SERVER_SUBSCRIPTIONS_ENDPOINT || `undefined`;