import {ApolloClient, ApolloProvider, createHttpLink, from, InMemoryCache, split} from '@apollo/client';
import {getMainDefinition} from '@apollo/client/utilities';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/App';
import {authTokenPassLink, prepareAuthTokenResetLink} from "./util/AuthHttpUtil";
import {WebSocketLink} from "./util/WebSocketLink";
import {authTokenStorage} from "./util/AuthTokenStorage";
import {graphqlEndpoint, subscriptionsEndpoint} from "./config";
import {onError} from "@apollo/client/link/error";

const errorLink = onError(({graphQLErrors, networkError}) => {
    graphQLErrors?.forEach(({message, locations, path}) => console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
    ));
    if (networkError) {
        console.log(`[Network error]: ${networkError}`);
    }
});

const httpLink = createHttpLink({
    uri: graphqlEndpoint,
})

const wsLink = new WebSocketLink({
    url: subscriptionsEndpoint,
    connectionParams: () => {
        const authToken = authTokenStorage.getToken();
        return {
            authorization: `Bearer ${authToken}`
        };
    },
});

const splitLink = split(
        ({query}) => {
            const definition = getMainDefinition(query);
            return (
                    definition.kind === 'OperationDefinition' &&
                    definition.operation === 'subscription'
            );
        },
        from([errorLink, wsLink]),
        from([errorLink, authTokenPassLink, prepareAuthTokenResetLink(), httpLink]),
);

export const apolloClient = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
    connectToDevTools: true,
});

ReactDOM.render(
        <React.StrictMode>
            <ApolloProvider client={apolloClient}>
                <App/>
            </ApolloProvider>
        </React.StrictMode>,
        document.getElementById('root')
);

// TODO: send to an analytics endpoint(https://bit.ly/CRA-vitals)
// reportWebVitals();