import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import React from 'react';
import {useGetCurrentUserQuery} from "../../gen-types";
import UserAvatar from "../avatar";
import Banner from "../banner";

type AccountButtonProps = {
    onClick?: React.MouseEventHandler;
}

export default function AccountButton(props: AccountButtonProps): React.ReactElement {

    const {loading, error, data} = useGetCurrentUserQuery()
    if (error) {
        return <Banner severity='error' text={`Error: ${error.message}`}/>;
    }
    return (
            <Box sx={{display: 'flex', alignItems: 'center', textAlign: 'center'}}>
                {
                    loading || !data
                            ? <CircularProgress sx={{flex: 1}} color="inherit"/>
                            : (
                                    <Tooltip title="Account settings">
                                        <IconButton onClick={props.onClick} size="small" sx={{ml: 2}}>
                                            <UserAvatar userId={data.currentUser.id}/>
                                        </IconButton>
                                    </Tooltip>
                            )
                }
            </Box>
    )
}