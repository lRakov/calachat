import {FetchResult} from "@apollo/client/link/core"
import LoginIcon from '@mui/icons-material/Login';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import React from 'react';
import GoogleButton from 'react-google-button';
import {GoogleLoginResponse, GoogleLoginResponseOffline, useGoogleLogin} from 'react-google-login';
import {DoAuthDocument, DoAuthMutation, useDoAuthMutation} from "../../gen-types";
import {apolloClient} from "../../index";
import {googleSignInClientId} from "../../config";
import {emitLogIn, emitLogOut} from "../../util/CustomEvent";

// TODO: move to google signIn
// @see https://dev.to/mremanuel/add-the-new-google-sign-in-to-your-react-app-p6m

export default function LoginButton(): React.ReactElement {

    const handleAuthSuccess = ({doAuth}: DoAuthMutation) => emitLogIn(doAuth)

    const handleAuthError = () => emitLogOut();

    const [doAuthMutation, {error: authError}] = useDoAuthMutation({
        onCompleted: handleAuthSuccess,
        onError: handleAuthError
    });

    const [open, setOpen] = React.useState<boolean>(false)

    const openDialog = () => setOpen(true);

    const closeDialog = () => setOpen(false);

    const handleLoginSuccess = (response: GoogleLoginResponse | GoogleLoginResponseOffline) => {
        if (isGoogleLoginResponse(response)) {
            doAuthMutation({
                variables: {tokenId: response.tokenId},
            })
            refreshTokenSetup(response);
            closeDialog()
        } else {
            alert(`illegal google auth configuration ${response.code}`)
        }
    }

    const handleLoginFail = (res: any) => {
        alert(`Fail: ${JSON.stringify(res)}`);  // TODO: handle logIn fail
    }

    const {signIn} = useGoogleLogin({
        clientId: googleSignInClientId,
        isSignedIn: true,
        cookiePolicy: 'single_host_origin',
        scope: 'profile email',
        accessType: 'offline',
        fetchBasicProfile: true,
        uxMode: 'popup',
        onSuccess: handleLoginSuccess,
        onFailure: handleLoginFail,
    })

    const refreshTokenSetup = (response: GoogleLoginResponse) => {

        const calcRefreshTime = (expiresIn: number) => (expiresIn || 3600 - 5 * 60) * 1000;

        const refreshToken = async () => {
            try {
                const newAuthResponse = await response.reloadAuthResponse();
                const result: FetchResult<DoAuthMutation> = await apolloClient.mutate({
                    mutation: DoAuthDocument,
                    variables: {
                        tokenId: newAuthResponse.id_token
                    }
                });
                if (result.data) {
                    handleAuthSuccess(result.data)
                    setTimeout(refreshToken, calcRefreshTime(newAuthResponse.expires_in));
                } else {
                    handleAuthError();
                }
            } catch (e) {
                handleAuthError();
            }
        };

        setTimeout(refreshToken, calcRefreshTime(response.tokenObj.expires_in));
    };

    return (
            <div>
                <IconButton color="inherit" onClick={openDialog}>
                    <LoginIcon/>
                </IconButton>
                <Dialog open={open}>
                    <DialogTitle>Welcome aboard</DialogTitle>
                    <DialogContent>
                        <DialogContentText>Available options</DialogContentText>
                        {
                            authError
                                    ? <Alert severity="error">Error: {authError}</Alert>
                                    : <List>{[<GoogleButton key='GoogleButton' onClick={signIn}/>]}</List>
                        }
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={closeDialog}>Cancel</Button>
                    </DialogActions>
                </Dialog>
            </div>
    )
}

function isGoogleLoginResponse(res: GoogleLoginResponse | GoogleLoginResponseOffline): res is GoogleLoginResponse {
    // eslint-disable-next-line
    return res.hasOwnProperty('profileObj');
}