import Logout from '@mui/icons-material/Logout';
import ListItemIcon from '@mui/material/ListItemIcon';
import MenuItem from '@mui/material/MenuItem';
import React from 'react';
import {useGoogleLogout} from 'react-google-login';
import {googleSignInClientId} from "../../config";
import {emitLogOut} from "../../util/CustomEvent";

export default function LogoutMenuItem(): React.ReactElement {

    const onGoogleLogoutSuccess = () => emitLogOut();

    const onFailure = () => emitLogOut();

    const {signOut} = useGoogleLogout({
        clientId: googleSignInClientId,
        onLogoutSuccess: onGoogleLogoutSuccess,
        onFailure: onFailure,
    });

    return (
            <MenuItem onClick={signOut}>
                <ListItemIcon>
                    <Logout fontSize="small"/>
                </ListItemIcon>
                Logout
            </MenuItem>
    )
}