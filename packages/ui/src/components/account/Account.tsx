import AccountMenu from "../account/AccountMenu";
import LoginButton from "../account/LoginButton";
import {useAppContext} from "../app/AppContext";

export default function Account(): React.ReactElement {

    const {authorized} = useAppContext()

    return authorized?.success
            ? <AccountMenu/>
            : <LoginButton/>
            ;
}