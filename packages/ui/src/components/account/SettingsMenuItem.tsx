import Settings from '@mui/icons-material/Settings';
import ListItemIcon from '@mui/material/ListItemIcon';
import MenuItem from '@mui/material/MenuItem';
import React from 'react';

export default function SettingsMenuItem(): React.ReactElement {

    return (
            <MenuItem>
                <ListItemIcon>
                    <Settings fontSize="small"/>
                </ListItemIcon>
                Settings(TODO)
            </MenuItem>
    )
}