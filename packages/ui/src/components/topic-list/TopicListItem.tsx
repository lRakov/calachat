import React from "react";
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import Tooltip from '@mui/material/Tooltip';
import Badge from '@mui/material/Badge';
import {Topic, useOnTopicUpdateSubscription} from "../../gen-types";
import {useAppContext} from "../app/AppContext";
import {
    emitFocusOnMessageInputEvent,
    emitResetHasNewMessagesEvent,
    useResetHasNewMessagesEvent
} from "../../util/CustomEvent";
import {topicCachePolicyHolder} from "../../util/TopicCachePolicy";
import TopicIcon from "../topic";

type TopicListItemProps = {
    topic: Topic,
    isCollapsed: boolean,
}

export default function TopicListItem({topic, isCollapsed}: TopicListItemProps): React.ReactElement {
    const {currentTopicId, setCurrentTopicId} = useAppContext()
    const [hasNewMessages, setHasNewMessages] = React.useState<boolean>(false)
    useOnTopicUpdateSubscription({
        variables: {
            topicId: topic.id
        },
        onSubscriptionData: ({subscriptionData}) => {
            if (subscriptionData.data?.OnTopicUpdate.hasNewMessages) {
                setHasNewMessages(true);
                if (currentTopicId && currentTopicId !== topic.id) {
                    topicCachePolicyHolder.denyCachingFor(topic.id)
                }
            }
        },
    });
    useResetHasNewMessagesEvent(topicId => {
        if (topicId === topic.id) {
            setHasNewMessages(false);
        }
    });

    const onClick = () => {
        setCurrentTopicId(topic.id);
        setTimeout(() => {
            emitResetHasNewMessagesEvent(topic.id)
            emitFocusOnMessageInputEvent()
        }, 100)
    }

    return (
            <Tooltip title={isCollapsed ? topic.displayName : ''} placement="right" arrow>
                <ListItemButton
                        selected={currentTopicId === topic.id}
                        onClick={onClick}>
                    <ListItemIcon>
                        <Badge color="secondary" variant="dot" invisible={!hasNewMessages}>
                            <TopicIcon topic={topic}/>
                        </Badge>
                    </ListItemIcon>
                    <ListItemText primary={topic.displayName}/>
                </ListItemButton>
            </Tooltip>
    );
}