import LinearProgress from '@mui/material/LinearProgress';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import {GetTopicsDocument, useGetTopicsQuery, useOnTopicAddedSubscription} from "../../gen-types";
import TopicListItem from './TopicListItem';
import {apolloClient} from "../../index";
import Banner from "../banner";

type TopicListProps = {
    isCollapsed: boolean,
}

export default function TopicList({isCollapsed}: TopicListProps): React.ReactElement {

    const {loading, error, data} = useGetTopicsQuery();
    useOnTopicAddedSubscription({
        onSubscriptionData: () => {
            apolloClient.refetchQueries({
                include: [GetTopicsDocument],
            });
        }
    });
    if (loading || !data) {
        return (
                <Box sx={{height: '100%'}}>
                    <LinearProgress color="primary"/>
                </Box>
        )
    }
    if (error) {
        return <Banner severity='error' text={`Error: ${error.message}`}/>;
    }
    return (
            <Box sx={{height: '100%'}}>
                <List>
                    {
                        data.topics.map((topic) => <TopicListItem key={topic.id} topic={topic} isCollapsed={isCollapsed}/>)
                    }
                </List>
            </Box>
    );
}