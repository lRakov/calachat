import {GetMessagesQuery} from "../../gen-types";

export type ChatBoxMessageItem = GetMessagesQuery['messagesBy'][0];

export function sortInplaceByIdAsc(items: ChatBoxMessageItem[]): ChatBoxMessageItem[] {
    return items.sort((a, b) => {
        const aN = +a.id;   // FIX: for some reasons returned ids have string type
        const bN = +b.id;
        if (aN > bN) {
            return 1;
        } else {
            return (bN > aN) ? -1 : 0;
        }
    });
}