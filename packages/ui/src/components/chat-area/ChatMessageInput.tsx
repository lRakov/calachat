import React from "react";
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import SendIcon from '@mui/icons-material/Send';
import {useDoCreateMessageMutation} from "../../gen-types";
import {useFocusOnMessageInputEvent} from "../../util/CustomEvent";

type MessageInputProps = {
    topicId: number,
}

const TextFieldStyle = {
    marginTop: '20px'
};

export function ChatMessageInput(props: MessageInputProps): React.ReactElement {
    const [text, setText] = React.useState("");

    const inputRef = React.useRef<HTMLInputElement>(null)

    useFocusOnMessageInputEvent(() => focusOnInput());

    const [doCreateMessageMutation, {loading}] = useDoCreateMessageMutation({
        onCompleted: () => {
            setText('');
            focusOnInput();
        }
    });

    function createMessage() {
        doCreateMessageMutation({
            variables: {
                topicId: props.topicId,
                text: text
            }
        })
    }

    const onClick = () => {
        createMessage();
    };

    const onEnter = (event: React.KeyboardEvent) => {
        if (event.key === 'Enter' && !event.shiftKey) {
            event.preventDefault();
            if (text) {
                createMessage();
            }
        }
    }

    const focusOnInput = () => inputRef.current && inputRef.current.focus();

    return (
            <TextField
                    sx={TextFieldStyle}
                    inputRef={inputRef}
                    disabled={loading}
                    label='Write a message...'
                    variant="outlined"
                    fullWidth
                    multiline maxRows={10}
                    value={text}
                    onChange={e => setText(e.target.value)}
                    onKeyDown={onEnter}
                    InputProps={{
                        endAdornment: (
                                <InputAdornment position="start">{
                                    (<IconButton
                                            color="inherit"
                                            disabled={!text || loading}
                                            onClick={onClick}>
                                        <SendIcon/>
                                    </IconButton>)
                                }</InputAdornment>
                        ),
                    }}
            />
    );
}