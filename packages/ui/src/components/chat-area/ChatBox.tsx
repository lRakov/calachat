import React from "react";
import {Virtuoso} from "react-virtuoso";
import LinearProgress from '@mui/material/LinearProgress';
import Fade from '@mui/material/Fade';
import Box from '@mui/material/Box';
import Banner from "../banner";
import {emitResetHasNewMessagesEvent} from "../../util/CustomEvent";
import ChatMessage from "./ChatMessage";
import {chatBoxComponents} from "./ChatBoxComponents";
import {useGetChatMessages} from "./ChatBoxHook";

type ChatListProps = {
    topicId: number,
}

export default function ChatBox({topicId}: ChatListProps): React.ReactElement {

    const {loading, error, messages} = useGetChatMessages(topicId);

    if (error) {
        return <Banner severity='error' text={`Error: ${error.message}`}/>;
    }
    if (loading) {
        return <Box sx={{flex: 1, height: '100%'}}>
            <Fade
                    in={loading}
                    style={{
                        transitionDelay: loading ? '800ms' : '0ms',
                    }}
                    unmountOnExit>
                <LinearProgress color="inherit"/>
            </Fade>
        </Box>
    }

    const atBottomStateChange = (atBottom: boolean) => {
        if (atBottom) {
            emitResetHasNewMessagesEvent(topicId);
        }
    }

    return (
            <Virtuoso
                    style={{flex: 1, height: '100%'}}
                    initialTopMostItemIndex={(messages.length || 1) - 1}
                    followOutput='auto'
                    alignToBottom={true}
                    components={chatBoxComponents}
                    itemContent={(index: number) => <ChatMessage message={messages[index]}/>}
                    data={messages}
                    atBottomStateChange={atBottomStateChange}
            />
    );
}