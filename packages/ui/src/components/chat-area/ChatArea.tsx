import React from 'react';
import Box from '@mui/material/Box';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import LeftMenuHeader from '../left-menu/LeftMenuHeader';
import {ChatMessageInput} from './ChatMessageInput';
import ChatBox from './ChatBox';
import {useAppContext} from '../app/AppContext';
import Banner from "../banner";

interface ChatAreaProps {
    sx?: SxProps<Theme>;
}

const rootBoxStyle: SxProps<Theme> = {
    display: 'flex',
    flexDirection: 'column',
};
const internalBoxStyle: SxProps<Theme> = {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
};

export default function ChatArea({sx = {}}: ChatAreaProps): React.ReactElement {

    const {currentTopicId, authorized} = useAppContext()

    if (!authorized?.success || !authorized?.userId) {
        return <Banner severity='info' text="Please, sign in"/>;
    }
    if (!currentTopicId) {
        return <Banner severity='info' text="Please, select a topic"/>;
    }
    const rootStyle = {...rootBoxStyle, ...sx};
    return (
            <Box sx={rootStyle}>
                <LeftMenuHeader/>
                <Box sx={internalBoxStyle}>
                    <ChatBox topicId={currentTopicId}/>
                    <ChatMessageInput topicId={currentTopicId}/>
                </Box>
            </Box>
    );
}