import React from 'react';
import Box from '@mui/material/Box';
import UserAvatar from '../avatar';
import {styled, Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import {useGetUserQuery} from '../../gen-types';
import Banner from '../banner';
import {formatDate} from "../../util/dateUtil";
import {ChatBoxMessageItem} from "./ChatMessageItem";

interface ChatMessageProps {
    message: ChatBoxMessageItem;
}

const avatarStyle: SxProps<Theme> = {
    margin: '8px'
};

const UserDisplayName = styled('div')(({theme}) => ({
    marginLeft: 0,
    ...theme.typography.body1,
}));
const MessageCreatedAt = styled('div')(({theme}) => ({
    marginLeft: 'auto',
    ...theme.typography.subtitle2,
}));
const MessageText = styled('div')(({theme}) => ({
    ...theme.typography.body2,
    backgroundColor: theme.palette.background.default,
    display: 'inline',
    whiteSpace: 'pre-line',
}));

export default function ChatMessage({message}: ChatMessageProps): React.ReactElement {

    const {error, data} = useGetUserQuery({
        variables: {
            userId: message.sourceId,
        },
    });
    if (error) {
        return <Banner severity='error' text={`Error: ${error.message}`}/>;
    }
    return (
            <Box sx={{
                display: 'flex',
                flexGrow: 1,
            }}>
                <UserAvatar userId={message.sourceId} sx={avatarStyle}/>
                <Box sx={{
                    display: 'flex',
                    flexGrow: 1,
                    flexDirection: 'column',
                }}>
                    <Box sx={{
                        display: 'flex',
                        flexGrow: 1,
                    }}>
                        <UserDisplayName>{data?.userBy.displayName || ''}</UserDisplayName>
                        <MessageCreatedAt>{formatDate(message.created_at)}</MessageCreatedAt>
                    </Box>
                    <MessageText>{message.text}</MessageText>
                </Box>
            </Box>
    );
}