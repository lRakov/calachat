import React from "react";
import {OnMessageAddedDocument, useGetMessagesQuery} from "../../gen-types";
import {ApolloError} from '@apollo/client'
import {topicCachePolicyHolder} from "../../util/TopicCachePolicy";
import {ChatBoxMessageItem, sortInplaceByIdAsc} from "./ChatMessageItem";

export function useGetChatMessages(topicId: number): {
    loading: boolean,
    error?: ApolloError,
    messages: ReadonlyArray<ChatBoxMessageItem>
} {
    const {subscribeToMore, data, loading, error} = useGetMessagesQuery({
        variables: {
            input: {
                topicId: topicId,
            }
        },
        fetchPolicy: topicCachePolicyHolder.getCachePolicyFor(topicId),
        onCompleted: () => {
            topicCachePolicyHolder.allowCachingFor(topicId)
        }
    })

    React.useEffect(() => {
        return subscribeToMore({
            document: OnMessageAddedDocument,
            variables: {
                topicId: topicId,
            },
            updateQuery: (prev, {subscriptionData}) => {
                if (!subscriptionData.data) {
                    return prev;
                }
                const newMessage = (subscriptionData.data as any).OnMessageAdded;
                const result = sortInplaceByIdAsc([...prev.messagesBy, newMessage]);
                return Object.assign({}, prev, {messagesBy: result});
            }
        });
    }, [topicId, subscribeToMore])
    return {
        loading,
        error,
        messages: data?.messagesBy || []
    };
}