import React from "react";
import {Components} from "react-virtuoso";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

export const chatBoxComponents: Components = {
    List: React.forwardRef<HTMLDivElement>((listProps: React.PropsWithChildren<any>, listRef: any) => {
        return (
                <List ref={listRef} style={{...listProps.style}}>
                    {listProps.children}
                </List>
        );
    }),
    Item: (ItemProps: React.PropsWithChildren<any>) => {
        return (
                <ListItem {...ItemProps} style={{margin: 0}}>
                    {ItemProps.children}
                </ListItem>
        );
    },
};