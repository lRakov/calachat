import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import {useDoJoinTopicMutation, useSearchTopicsQuery} from "../../gen-types";
import TopicCreationDialog, {TopicCreationDialogValue} from "./TopicCreationDialog";
import {useAppContext} from "../app/AppContext";
import {emitFocusOnMessageInputEvent} from "../../util/CustomEvent";

// TODO: user input validation on server side

function useFetchTopics() {
    const [inputText, setInputText] = React.useState<string>('');
    const [requestText, setRequestText] = React.useState<string>('');
    const {data, loading} = useSearchTopicsQuery({
        variables: {
            text: requestText,
        }
    })
    React.useEffect(() => {
        const delayDebounceFn = setTimeout(() => {
            setRequestText(inputText)
        }, 500)
        return () => clearTimeout(delayDebounceFn)
    }, [inputText])
    return {
        topicsLoading: loading,
        topics: data?.topicsBy || [],
        onInputChange: (event: React.SyntheticEvent, value: string) => setInputText(value)
    };
}

const defaultSelection: TopicCreationDialogValue = {
    displayName: ''
};

export default function TopicSelector(): React.ReactElement | null {

    const {setCurrentTopicId} = useAppContext()
    const {topicsLoading, topics, onInputChange} = useFetchTopics();
    const [selectedValue, setSelectedValue] = React.useState<TopicOptionType>(defaultSelection);
    const [dialogOpen, setDialogOpen] = React.useState<boolean>(false);
    const [dialogValue, setDialogValue] = React.useState<TopicCreationDialogValue>(defaultSelection);

    const [doJoinTopicMutation] = useDoJoinTopicMutation({
        onCompleted: (data) => {
            setCurrentTopicId(data.doJoinTopic.topicId)
            setSelectedValue(defaultSelection)
            emitFocusOnMessageInputEvent()
        }
    });

    const handleClose = () => {
        setDialogOpen(false);
    };

    const handleSubmit = (topicId: number) => {
        doJoinTopicMutation({
            variables: {topicId: topicId},
        })
        handleClose();
    };

    const onChange = (event: React.SyntheticEvent, newValue: any) => {
        event.preventDefault();
        if (newValue && newValue.inputValue && !newValue.id) {
            setDialogOpen(true);
            setDialogValue({
                displayName: newValue.inputValue,
            });
        } else {
            setSelectedValue(newValue);
        }
        if (newValue && newValue.id) {
            doJoinTopicMutation({
                variables: {topicId: newValue.id},
            })
        }
    };

    return (
            <React.Fragment>
                <Autocomplete
                        selectOnFocus
                        handleHomeEndKeys
                        freeSolo
                        sx={{width: '100%'}}
                        onInputChange={onInputChange}
                        onChange={onChange}
                        value={selectedValue}
                        filterOptions={(options, params) => {
                            if (params.inputValue !== ''
                                    && !topicsLoading
                                    && !options.some(it => it.displayName.toLowerCase() === params.inputValue.toLowerCase())) {
                                options.push({
                                    inputValue: params.inputValue,
                                    displayName: `Create "${params.inputValue.substring(0, 10)}"`,
                                });
                            }
                            return options;
                        }}
                        loading={topicsLoading}
                        options={topics.map(topic => (topic as TopicOptionType))}
                        getOptionLabel={(option) => {
                            if (typeof option === 'string') {
                                return option;
                            }
                            if (option.inputValue) {
                                return option.inputValue;
                            }
                            return option.displayName;
                        }}
                        renderOption={(props, option) => <li {...props}>{option.displayName}</li>}
                        renderInput={(params) => (<TextField {...params} label="Find or create a topic"/>)}
                />
                <TopicCreationDialog
                        open={dialogOpen}
                        defaultValue={dialogValue}
                        onClose={handleClose}
                        onSubmit={handleSubmit}/>
            </React.Fragment>
    );
}

interface TopicOptionType {
    inputValue?: string;
    id?: number,
    displayName: string
}