import * as React from 'react';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import {DoCreateTopicMutation, useDoCreateTopicMutation} from "../../gen-types";
import {useForm} from 'react-hook-form';
import {zodResolver} from '@hookform/resolvers/zod';
import * as z from 'zod';
import {ApolloError} from '@apollo/client';

export type TopicCreationDialogValue = {
    displayName: string
}

type TopicCreationDialogProps = {
    defaultValue: TopicCreationDialogValue,
    open: boolean,
    onClose: () => void,
    onSubmit: (topicId: number) => void,
}

const schema = z.object({
    displayName: z.string().nonempty({message: 'Required'}).min(3).max(20).regex(/\w+/),
});

export default function TopicCreationDialog(props: TopicCreationDialogProps): React.ReactElement {

    const {
        register,
        handleSubmit,
        formState: {errors, isDirty, isValid, isSubmitting, isValidating},
    } = useForm<TopicCreationDialogValue>({
        mode: 'all',
        shouldUnregister: true,
        resolver: zodResolver(schema),
    });
    const [doCreateTopicMutation] = useDoCreateTopicMutation({
        onCompleted: (data: DoCreateTopicMutation) => {
            props.onSubmit(data.doCreateTopic.id);
        },
        onError: (error: ApolloError) => {
            alert(error)
        }
    });

    const onSubmit = (data: TopicCreationDialogValue) => {
        doCreateTopicMutation({
            variables: {displayName: data.displayName.trim()}
        })
    };

    return (
            <React.Fragment>
                <Dialog open={props.open} onClose={props.onClose}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <DialogTitle>Create a new Topic</DialogTitle>
                        <DialogContent>
                            <TextField
                                    autoFocus
                                    autoComplete='off'
                                    margin="dense"
                                    helperText="3-20 alphanumeric symbols"
                                    label="Topic name"
                                    type="text"
                                    variant="standard"
                                    id="displayName"
                                    defaultValue={props.defaultValue.displayName}
                                    error={!isValid || !!errors.displayName?.message}
                                    {...register('displayName')}/>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                    onClick={props.onClose}
                                    disabled={isSubmitting || isValidating}
                            >Cancel</Button>
                            <Button
                                    type="submit"
                                    disabled={isSubmitting || isValidating || !isDirty || !isValid}
                            >Add</Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </React.Fragment>
    );
}