import Box from '@mui/material/Box';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import ChatArea from "./chat-area";

const rootBoxStyle: SxProps<Theme> = {
    flexGrow: 1,
    height: "100vh"
};
const chatAreaStyle: SxProps<Theme> = {
    height: '100%',
    padding: '12px 24px',
};

export default function MainArea(): React.ReactElement {
    return (
            <Box component="main" sx={rootBoxStyle}>
                <ChatArea sx={chatAreaStyle}/>
            </Box>
    );
}
