import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import {useGetUserQuery} from "../../gen-types";
import Banner from "../banner";
import {stringToColor, toInitials} from "./util";

interface UserAvatarProps {
    userId: number;
    sx?: SxProps<Theme>;
}

const avatarStyle = {
    width: 32,
    height: 32,
};

export default function UserAvatar({userId, sx = {}}: UserAvatarProps): React.ReactElement {

    const {loading, error, data} = useGetUserQuery({
        variables: {
            userId: userId,
        },
    });
    if (loading || !data) {
        return <Box sx={{...avatarStyle, ...sx}}/>;
    }
    if (error) {
        return <Banner severity='error' text={`Error: ${error.message}`}/>;
    }
    const name = data.userBy.displayName;
    const avatar = data.userBy.avatar || undefined;
    const bgcolor = !avatar ? stringToColor(name) : 'inherit';
    return (
            <Avatar src={avatar}
                    sx={{bgcolor: bgcolor, ...avatarStyle, ...sx}}
                    children={toInitials(name)}
            />
    );
}