export function stringToColor(string: string) {
    let hash = 0;
    /* eslint-disable no-bitwise */
    for (let i = 0; i < string.length; i += 1) {
        hash = string.charCodeAt(i) + ((hash << 5) - hash);
    }
    let color = '#';
    for (let i = 0; i < 3; i += 1) {
        const value = (hash >> (i * 8)) & 0xff;
        color += `00${value.toString(16)}`.substr(-2);
    }
    /* eslint-enable no-bitwise */
    return color;
}

export function toInitials(name: string) {
    const strings: string[] = name.split(' ');
    const b1 = strings.length > 0 ? strings[0][0] : '';
    const b2 = strings.length > 1 ? strings[1][0] : '';
    return b1 + b2;
}