import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Divider from '@mui/material/Divider';
import MuiDrawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import {CSSObject, styled, Theme, useTheme} from '@mui/material/styles';
import {useAppContext} from "../app/AppContext";
import LeftMenuHeader from './LeftMenuHeader';
import TopicList from "../topic-list";
import TopicSelector from "../topic-selector";

export const leftMenuWidth = 240; // TODO: get rid of bad practice

const openedMixin = (theme: Theme): CSSObject => ({
    width: leftMenuWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});

const closedMixin = (theme: Theme): CSSObject => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(9)} + 1px)`,
    },
});

const Drawer = styled(MuiDrawer, {
    shouldForwardProp: (prop) => prop !== 'isCollapsed' && prop !== 'setIsCollapsed'
})<LeftMenuProps>(
        ({theme, isCollapsed}) => ({
            width: leftMenuWidth,
            flexShrink: 0,
            whiteSpace: 'nowrap',
            boxSizing: 'border-box',
            ...(!isCollapsed && {
                ...openedMixin(theme),
                '& .MuiDrawer-paper': openedMixin(theme),
            }),
            ...(isCollapsed && {
                ...closedMixin(theme),
                '& .MuiDrawer-paper': closedMixin(theme),
            }),
        }),
);

type LeftMenuProps = {
    isCollapsed: boolean,
    setIsCollapsed: (open: boolean) => void
}

export default function LeftMenu({isCollapsed, setIsCollapsed}: LeftMenuProps): React.ReactElement | null {

    const closeLeftMenu = () => {
        setIsCollapsed(true);
    };

    const theme = useTheme();
    const {authorized} = useAppContext()

    return (
            <Drawer variant="permanent" isCollapsed={isCollapsed} setIsCollapsed={setIsCollapsed}>
                <LeftMenuHeader>
                    {
                        authorized?.success && <TopicSelector/>
                    }
                    <IconButton onClick={closeLeftMenu}>
                        {
                            theme.direction === 'rtl' ? <ChevronRightIcon/> : <ChevronLeftIcon/>
                        }
                    </IconButton>
                </LeftMenuHeader>
                <Divider/>
                {
                    authorized?.success && <TopicList isCollapsed={isCollapsed}/>
                }
            </Drawer>
    );
}