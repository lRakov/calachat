import React from 'react'
import {DoAuthMutation} from "../../gen-types";

export type AuthData = DoAuthMutation["doAuth"];

type AppState = {
    authorized?: AuthData,
    setAuthorized: (val: AuthData) => void,
    currentTopicId?: number,
    setCurrentTopicId: (val?: number) => void,
}

export const AppContext = React.createContext<AppState>({
    authorized: undefined,
    setAuthorized: () => {
        // do nothing
    },
    currentTopicId: undefined,
    setCurrentTopicId: () => {
        // do nothing
    },
})

export function useAppContext(): AppState {
    return React.useContext(AppContext)
}

export function useAppState(): AppState {
    const [authorized, setAuthorized] = React.useState<AuthData>({
        success: false,
    })
    const [currentTopicId, setCurrentTopicId] = React.useState<number | undefined>(undefined)
    return React.useMemo(() => ({
         authorized, setAuthorized, currentTopicId, setCurrentTopicId
    }), [authorized, setAuthorized, currentTopicId, setCurrentTopicId]);
}