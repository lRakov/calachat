import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import AppBox from './AppBox';
import {AppContext, useAppState} from "./AppContext";

export default function App(): React.ReactElement {
    return (
            <AppContext.Provider value={useAppState()}>
                <CssBaseline/>
                <AppBox/>
            </AppContext.Provider>
    );
}