import React from 'react';
import Box from '@mui/material/Box';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import LeftMenu from '../left-menu';
import MainArea from '../MainArea';
import AppBar from './AppBar';
import {useWindowWidth} from '@react-hook/window-size'
import {LogInData, useLogInEvent, useLogOutEvent} from "../../util/CustomEvent";
import {authTokenStorage} from "../../util/AuthTokenStorage";
import {useAppContext} from "./AppContext";
import {apolloClient} from "../../index";

const AppBoxStyle: SxProps<Theme> = {
    display: 'flex'
};

export default function AppBox(): React.ReactElement {
    const {authorized, setAuthorized} = useAppContext();
    const windowWidth = useWindowWidth();
    const isSmallViewport = windowWidth < 800;
    const [isLeftMenuCollapsed, setIsLeftMenuCollapsed] = React.useState<boolean>(!authorized || isSmallViewport);

    useLogInEvent((data: LogInData) => {
        authTokenStorage.setToken(data.authToken || undefined);
        setAuthorized(data);
        setIsLeftMenuCollapsed(isSmallViewport);
    });
    useLogOutEvent(() => {
        apolloClient.resetStore().then(() => {
            authTokenStorage.setToken();
            setAuthorized({
                success: false,
            });
            setIsLeftMenuCollapsed(true);
        });
    });

    return (
            <Box sx={AppBoxStyle}>
                <AppBar isLeftMenuCollapsed={isLeftMenuCollapsed} setIsLeftMenuCollapsed={setIsLeftMenuCollapsed}/>
                <LeftMenu isCollapsed={isLeftMenuCollapsed} setIsCollapsed={setIsLeftMenuCollapsed}/>
                <MainArea/>
            </Box>
    );
}