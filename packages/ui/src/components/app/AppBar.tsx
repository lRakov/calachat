import MenuIcon from '@mui/icons-material/Menu';
import MuiAppBar, {AppBarProps as MuiAppBarProps} from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import {styled, Theme} from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import {SxProps} from '@mui/system';
import Account from "../account";
import {leftMenuWidth} from '../left-menu/LeftMenu';

interface AppBarProps extends MuiAppBarProps {
    isLeftMenuCollapsed: boolean,
    setIsLeftMenuCollapsed: (open: boolean) => void
}

const AppBarComponent = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'isLeftMenuCollapsed' && prop !== 'setIsLeftMenuCollapsed',
})<AppBarProps>(({theme, isLeftMenuCollapsed}) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(!isLeftMenuCollapsed && {
        marginLeft: leftMenuWidth,
        width: `calc(100% - ${leftMenuWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const headerStyle: SxProps<Theme> = {
    width: '100%'
};

function menuIconStyle(isLeftMenuCollapsed: boolean): SxProps<Theme> {
    return {
        ...(!isLeftMenuCollapsed && {display: 'none'}),
    };
}

export default function AppBar({isLeftMenuCollapsed, setIsLeftMenuCollapsed}: AppBarProps): React.ReactElement {
    return (
            <AppBarComponent position="fixed"
                             isLeftMenuCollapsed={isLeftMenuCollapsed}
                             setIsLeftMenuCollapsed={setIsLeftMenuCollapsed}>
                <Toolbar>
                    <IconButton
                            color="inherit"
                            onClick={() => setIsLeftMenuCollapsed(false)}
                            edge="start"
                            sx={menuIconStyle(isLeftMenuCollapsed)}>
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" noWrap sx={headerStyle}>Calachat</Typography>
                    <Account/>
                </Toolbar>
            </AppBarComponent>
    );
}