import React from 'react';
import Box from '@mui/material/Box';
import Alert, {AlertColor} from '@mui/material/Alert';

type BannerProps = {
    text: string,
    severity?: AlertColor,
}

export default function Banner(props: BannerProps): React.ReactElement {
    return (
            <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                height: '100%',
            }}>
                <Alert severity={props.severity}>{props.text}</Alert>
            </Box>
    );
}