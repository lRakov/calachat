import Avatar from '@mui/material/Avatar';
import {Theme} from '@mui/material/styles';
import {SxProps} from '@mui/system';
import {Topic} from "../../gen-types";
import {stringToColor, toInitials} from "../avatar/util";

interface TopicIconProps {
    topic: Topic;
    sx?: SxProps<Theme>;
}

const avatarStyle = {
    width: 32,
    height: 32,
};

export default function TopicIcon({topic, sx = {}}: TopicIconProps): React.ReactElement {
    const name = topic.displayName;
    const bgcolor = stringToColor(name);
    return (
            <Avatar sx={{bgcolor: bgcolor, ...avatarStyle, ...sx}}
                    children={toInitials(name)}
                    variant='rounded'
            />
    );
}