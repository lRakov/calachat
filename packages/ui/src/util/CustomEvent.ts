import {emitCustomEvent, useCustomEventListener} from 'react-custom-events';
import {DoAuthMutation} from "../gen-types";

enum CustomEvent {
    LOG_IN = 'LogIn',
    LOG_OUT = 'LogOut',
    FOCUS_ON_MESSAGE_INPUT = 'FocusOnMessageInput',
    RESET_HAS_NEW_MESSAGES = 'ResetHasNewMessages',
}

export type LogInData = NonNullable<DoAuthMutation['doAuth']>;

export function emitLogIn(data: LogInData): void {
    emitCustomEvent(CustomEvent.LOG_IN, data);
}

export function useLogInEvent(on: (data: LogInData) => void): void {
    useCustomEventListener(CustomEvent.LOG_IN, on);
}

export function emitLogOut(): void {
    emitCustomEvent(CustomEvent.LOG_OUT);
}

export function useLogOutEvent(on: () => void): void {
    useCustomEventListener(CustomEvent.LOG_OUT, on);
}

export function emitFocusOnMessageInputEvent(): void {
    emitCustomEvent(CustomEvent.FOCUS_ON_MESSAGE_INPUT);
}

export function useFocusOnMessageInputEvent(on: () => void): void {
    useCustomEventListener(CustomEvent.FOCUS_ON_MESSAGE_INPUT, on);
}

export function emitResetHasNewMessagesEvent(topicId: number): void {
    emitCustomEvent(CustomEvent.RESET_HAS_NEW_MESSAGES, {topicId: topicId});
}

export function useResetHasNewMessagesEvent(on: (topicId: number) => void): void {
    useCustomEventListener(CustomEvent.RESET_HAS_NEW_MESSAGES, (data: any) => on(data.topicId));
}