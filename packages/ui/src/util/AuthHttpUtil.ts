import {ApolloLink, GraphQLRequest} from '@apollo/client';
import {setContext} from '@apollo/client/link/context';
import {ErrorResponse, onError} from "@apollo/client/link/error";
import {ServerError} from '@apollo/client/link/utils';
import {ServerParseError} from '@apollo/client/link/http';
import {authTokenStorage} from "./AuthTokenStorage";
import {emitLogOut} from "./CustomEvent";

export function prepareAuthTokenResetLink(onAuthFail: (error: ServerError) => void = () => undefined): ApolloLink {
    return onError((resp: ErrorResponse) => {
        if (isAuthError(resp.networkError)) {
            onAuthFail(resp.networkError)
            emitLogOut()
        }
    });
}

export const authTokenPassLink: ApolloLink = setContext((operation: GraphQLRequest, prevContext: any) => {
    const authToken = authTokenStorage.getToken();
    return authToken ? {
        headers: {
            ...prevContext.headers,
            authorization: `Bearer ${authToken}`
        }
    } : prevContext;
});

function isAuthError(networkError?: Error | ServerError | ServerParseError | null): networkError is ServerError {
    return isServerError(networkError) && networkError.statusCode === 401;
}

function isServerError(networkError?: Error | ServerError | ServerParseError | null): networkError is (ServerError | ServerParseError) {
    return 'statusCode' in (networkError || {});
}
