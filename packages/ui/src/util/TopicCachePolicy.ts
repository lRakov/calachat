import {FetchPolicy} from "@apollo/client";


interface TopicCachePolicyHolder {
    getCachePolicyFor(topicId: number): FetchPolicy;
    allowCachingFor(topicId: number): void
    denyCachingFor(topicId: number): void;
}

export const topicCachePolicyHolder: TopicCachePolicyHolder = {
    getCachePolicyFor(topicId: number) {
        const holderElement = holder[topicId];
        return holderElement === undefined ? 'cache-first' : 'network-only';
    },

    allowCachingFor(topicId: number): void {
        holder[topicId] = undefined;
    },

    denyCachingFor(topicId: number): void {
        holder[topicId] = false;
    },
}

const holder: Record<number, boolean | undefined> = {};
