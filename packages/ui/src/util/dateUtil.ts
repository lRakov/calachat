import localizedFormat from 'dayjs/plugin/localizedFormat';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import isToday from 'dayjs/plugin/isToday';

dayjs.extend(localizedFormat)
dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.extend(isToday)

const DTF_LOCAL_DATE_TIME = 'll LTS';
const DTF_LOCAL_TIME = 'LTS';

export type TemplateSelector = (date: dayjs.Dayjs) => string;

export function formatDate(
    input: Date,
    templateSelector: TemplateSelector = createdAtTemplateSelector
): string {
    const date = dayjs(input);
    return date.format(templateSelector(date));
}

export const createdAtTemplateSelector: TemplateSelector = (date) =>
    date.isToday()
        ? DTF_LOCAL_TIME
        : DTF_LOCAL_DATE_TIME;

export function toLocalFormat(input: Date): string {
    return dayjs(input).format(DTF_LOCAL_DATE_TIME);
}