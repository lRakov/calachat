import {LocalStorageKey} from "./LocalStorageKey";

interface AuthTokenStorage {
    getToken(): string | null
    setToken(val?: string): void
}

export const authTokenStorage: AuthTokenStorage = {

    getToken(): string | null {
        return localStorage.getItem(LocalStorageKey.LS_AUTH_TOKEN)
    },

    setToken(val?: string): void {
        if (val) {
            localStorage.setItem(LocalStorageKey.LS_AUTH_TOKEN, val)
        }
        else {
            localStorage.removeItem(LocalStorageKey.LS_AUTH_TOKEN)
        }
    }
}