import gql from 'graphql-tag';

export const schema = gql`

    scalar DateTime
    scalar Void

    schema {
        query: Query
        mutation: Mutation
        subscription: Subscription
    }

    type Query {
        currentUser: CurrentUser!
        topics: [Topic!]!
        topicsBy(text: String): [Topic!]!
        messagesBy(input: MessagesByInput!): [Message!]!
        userBy(userId: ID!): User!
    }

    input MessagesByInput {
        topicId: ID!
        minId: ID = null
        maxId: ID = null
        limit: Int = 100
    }

    type Mutation {
        doAuth(tokenId: String): AuthResult!
        doCreateTopic(displayName: String!): Topic!
        doCreateMessage(topicId: ID!, text: String!): Message!
        doJoinTopic(topicId: ID!): TopicMember!
    }

    type Subscription {
        OnTopicAdded: TopicMember!
        OnTopicUpdate(topicId: ID!): TopicUpdate!
        OnMessageAdded(topicId: ID!): Message!
    }

    type AuthResult {
        success: Boolean!
        userId: ID
        authToken: String
        error: String
    }

    interface IIdentifiable {
        id: ID!
    }

    interface INamed {
        displayName: String!
    }

    type CurrentUser implements IUser & IIdentifiable & INamed {
        id: ID!
        displayName: String!
        firstName: String
        lastName: String
        lastSeen: DateTime
        avatar: String
    }

    interface IUser implements IIdentifiable & INamed {
        id: ID!
        displayName: String!
        firstName: String
        lastName: String
        lastSeen: DateTime
        avatar: String
    }

    type User implements IUser & IIdentifiable & INamed {
        id: ID!
        displayName: String!
        firstName: String
        lastName: String
        lastSeen: DateTime
        avatar: String
    }

    type Topic implements IIdentifiable & INamed {
        id: ID!
        displayName: String!
        members: [User!]!
        status: TopicStatus!
    }

    enum TopicStatus {
        DRAFT,
        ACTIVE,
        CLOSED,
        MUTED,
    }

    type TopicMember {
        topicId: ID!
        userId: ID!
    }

    type TopicUpdate {
        topicId: ID!
        hasNewMessages: Boolean!
    }
    
    type Message implements IIdentifiable {
        id: ID!
        topicId: ID!
        sourceId: ID!
        text: String!
        created_at: DateTime!  # UTC
    }
`
