import gql from 'graphql-tag';

export const GetMessages = gql`

    query GetMessages($input: MessagesByInput!) {
        messagesBy(input: $input) {
            id
            sourceId
            text
            created_at
        }
    }
`;