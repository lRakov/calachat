import gql from 'graphql-tag';

export const doAuth = gql`

    mutation doAuth($tokenId: String!) {
        doAuth(tokenId: $tokenId) {
            success
            userId
            authToken
            error
        }
    }
`;