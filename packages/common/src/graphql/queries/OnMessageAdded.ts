import gql from 'graphql-tag';

export const OnMessageAdded = gql`
    
    subscription OnMessageAdded($topicId: ID!) {
        OnMessageAdded(topicId: $topicId) {
            id
            topicId
            sourceId
            text
            created_at
        }
    }
`;