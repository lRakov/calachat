import gql from 'graphql-tag';

export const GetTopics = gql`

    query GetTopics {
        topics {
            id
            displayName
            status
            members {
                id
                displayName
            }
        }
    }
`;