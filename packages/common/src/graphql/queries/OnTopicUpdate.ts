import gql from 'graphql-tag';

export const OnTopicUpdate = gql`
    
    subscription OnTopicUpdate($topicId: ID!) {
        OnTopicUpdate(topicId: $topicId) {
            topicId
            hasNewMessages
        }
    }
`;