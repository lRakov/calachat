import gql from 'graphql-tag';

export const GetTopics = gql`
    
    subscription OnTopicAdded {
        OnTopicAdded {
            userId,
            topicId,
        }
    }
`;