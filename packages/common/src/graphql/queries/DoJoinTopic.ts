import gql from 'graphql-tag';

export const doJoinTopic = gql`

    mutation doJoinTopic($topicId: ID!) {
        doJoinTopic(topicId: $topicId) {
            topicId,
            userId,
        }
    }
`;