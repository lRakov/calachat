import gql from 'graphql-tag';

export const GetUser = gql`

    query GetUser($userId: ID!) {
        userBy(userId: $userId) {
            id
            displayName
            avatar
            lastSeen
        }
    }
`;