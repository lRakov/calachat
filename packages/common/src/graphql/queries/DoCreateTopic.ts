import gql from 'graphql-tag';

export const doCreateTopic = gql`

    mutation doCreateTopic($displayName: String!) {
        doCreateTopic(displayName: $displayName) {
            id,
            displayName,
            status,
        }
    }
`;