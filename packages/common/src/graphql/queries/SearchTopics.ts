import gql from 'graphql-tag';

export const SearchTopics = gql`

    query SearchTopics($text: String) {
        topicsBy(text: $text) {
            id
            displayName
            status
        }
    }
`;