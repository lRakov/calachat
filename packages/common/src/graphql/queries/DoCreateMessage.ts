import gql from 'graphql-tag';

export const doCreateMessage = gql`

    mutation doCreateMessage($topicId: ID!, $text: String!) {
        doCreateMessage(topicId: $topicId, text: $text) {
            id
            topicId
            sourceId
            text
            created_at
        }
    }
`;