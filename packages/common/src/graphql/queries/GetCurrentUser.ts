import gql from 'graphql-tag';

export const GetCurrentUser = gql`

    query GetCurrentUser {
        currentUser { 
            id
            displayName
            firstName
            lastName
            lastSeen
            avatar
        }
    }
`;