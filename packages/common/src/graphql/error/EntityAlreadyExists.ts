import {ApolloError} from 'apollo-server-errors';

export class EntityAlreadyExists extends ApolloError {
    constructor(
        message: string,
        entityType: string,
        entityId: number,
    ) {
        super(message, 'ENTITY_ALREADY_EXISTS');
        Object.defineProperty(this, 'entityType', {value: entityType});
        Object.defineProperty(this, 'entityId', {value: entityId});
    }
}