FROM node:17-alpine

#TODO: security(non root)

COPY ./packages/common /packages/common
COPY ./packages/server /packages/server
COPY ./packages/ui /packages/ui
COPY ./package.json /package.json
COPY ./yarn.lock /yarn.lock
COPY ./tsconfig.json /tsconfig.json
COPY ./codegen.yml /codegen.yml
COPY ./.env.production /.env.production
COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
RUN yarn install --frozen-lockfile
RUN yarn codegen
RUN export NODE_OPTIONS=--openssl-legacy-provider && yarn build:prod

CMD ["/docker-entrypoint.sh"]

EXPOSE 3000:3000